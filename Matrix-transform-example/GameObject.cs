﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace Matrix_transform_example
{
    class GameObject
    {
        //Data
        private Model model;
        private Matrix[] transforms;

        public Vector3 position;
        public Vector3 scale = Vector3.One;
        public Vector3 rotation;


        //behaviour
        public void LoadModel(ContentManager content, string filename)
        {
            model = content.Load<Model>(filename);

            transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
        }
        //----
        public void draw()
        {
            foreach(ModelMesh mesh in model.Meshes)
            {
                foreach(BasicEffect effect in mesh.Effects)
                {
                    //model coordinates - starting point before any transforms
                    effect.World = transforms[mesh.ParentBone.Index];

                    //scale
                    effect.World *= Matrix.CreateScale(scale);


                    //roatation
                    //roate our models in game worlds
                    effect.World *= Matrix.CreateRotationX(rotation.X); // rotate round x axis
                    effect.World *= Matrix.CreateRotationY(rotation.Y); // rotate round y axis
                    effect.World *= Matrix.CreateRotationZ(rotation.Z); // rotate around z axis


                    //translation/position
                    //move our model to the correct place in the game world
                    effect.World *= Matrix.CreateTranslation(position);





                    effect.View = Matrix.CreateLookAt(
                        new Vector3(0, 500, -500), // camera position
                        Vector3.Zero, // target 
                        Vector3.Up // the direction of up
                        );

                    // takes the 3D images and covert it to 2D
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(
                        MathHelper.ToRadians(45), // field of view 
                        16f / 9f, // aspect raito 
                        1f, // near clipping plane
                        100000f); // far clipping plane

                    // some basic lighting
                    effect.LightingEnabled = true;
                    effect.Alpha = 1.0f;
                    effect.AmbientLightColor = new Vector3(1.0f);
                }

                //draw the current mesh using the effect we set up
                mesh.Draw();
            }
        }
        //----
    }
}
